package com.maxima.homework27.controllers;


import com.maxima.homework27.dto.ActorDto;
import com.maxima.homework27.services.ActorsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@Controller
public class ActorsController {
    @Autowired
    private ActorsService actorsService;

    @RequestMapping("/actors")
    public String getActorsPage() {
        return "actors";
    }

    @RequestMapping(value = "/actors", method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    @ResponseStatus(HttpStatus.CREATED)
    public List<ActorDto> addActor(@RequestBody ActorDto actor) {
        actorsService.addActor(actor);
        return actorsService.getAllActors();
    }

}
