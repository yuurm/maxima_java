package com.maxima.homework27.services;

import com.maxima.homework27.dto.AccountDto;
import com.maxima.homework27.repositories.AccountsRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class UsersServiceImpl implements UsersService {

    private final AccountsRepository accountsRepository;

    @Override
    public List<AccountDto> getAllUsers() {
        return AccountDto.from(accountsRepository.findAll());
    }
}

