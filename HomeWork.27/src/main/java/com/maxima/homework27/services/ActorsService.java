package com.maxima.homework27.services;


import com.maxima.homework27.dto.ActorDto;

import java.util.List;

public interface ActorsService {
    void addActor(ActorDto actor);
    List<ActorDto> getAllActors();

}
