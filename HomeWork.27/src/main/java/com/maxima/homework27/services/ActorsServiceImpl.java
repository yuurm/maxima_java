package com.maxima.homework27.services;

import static com.maxima.homework27.dto.ActorDto.from;
import com.maxima.homework27.dto.ActorDto;
import com.maxima.homework27.models.Actor;
import com.maxima.homework27.repositories.ActorsRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;




@Service
@RequiredArgsConstructor
public class ActorsServiceImpl implements ActorsService {

    private final ActorsRepository actorsRepository;


    @Override
    public void addActor(ActorDto actor) {
        Actor newActor = Actor.builder()
                .id(actor.getId())
                .name(actor.getName())
                .build();
        actorsRepository.save(newActor);


    }

    @Override
    public List<ActorDto> getAllActors() {
        return from(actorsRepository.findAll());
    }

}
