package com.maxima.homework27.services;




import static com.maxima.homework27.dto.FilmDto.from;

import com.maxima.homework27.dto.ActorDto;
import com.maxima.homework27.dto.FilmDto;
import com.maxima.homework27.models.Actor;
import com.maxima.homework27.models.Film;
import com.maxima.homework27.repositories.ActorsRepository;
import com.maxima.homework27.repositories.FilmsRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
@RequiredArgsConstructor
public class FilmsServiceImpl implements FilmsService {

    private final ActorsRepository actorsRepository;
    private final FilmsRepository filmsRepository;

    @Override
    public void addFilm(FilmDto film) {
        Film newFilm = Film.builder()
                .id(film.getId())
                .name(film.getName())
                .build();
        filmsRepository.save(newFilm);
    }

    @Override
    public List<FilmDto> getAllFilms() {
        return from(filmsRepository.findAll());
    }

    @Override
    public void addActorToFilm(Long filmId, ActorDto actorForm) {
        Film film = filmsRepository.getById(filmId);
        Actor actor = actorsRepository.getById(actorForm.getId());
        actor.setFilm(film);
        actorsRepository.save(actor);
    }

    @Override
    public List<ActorDto> getActorsByFilm(Long filmId) {
        return ActorDto.from(actorsRepository.findAllByFilm(filmId));
    }

    @Override
    public List<ActorDto> getActorsWithoutFilm() {
        return ActorDto.from(actorsRepository.findAllByFilmIsNull());
    }
}
