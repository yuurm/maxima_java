package com.maxima.homework27.services;


import com.maxima.homework27.dto.ActorDto;
import com.maxima.homework27.dto.FilmDto;

import java.util.List;

public interface FilmsService {
    void addFilm(FilmDto film);
    List<FilmDto> getAllFilms();
    void addActorToFilm(Long filmId, ActorDto actor);
    List<ActorDto> getActorsByFilm(Long filmId);
    List<ActorDto> getActorsWithoutFilm();

}
