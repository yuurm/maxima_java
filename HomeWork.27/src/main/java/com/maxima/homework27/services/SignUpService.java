package com.maxima.homework27.services;

import com.maxima.homework27.dto.SignUpForm;

public interface SignUpService {
    void signUp(SignUpForm form);
}

