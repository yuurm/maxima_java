package com.maxima.homework27.services;

import com.maxima.homework27.dto.AccountDto;

import java.util.List;

public interface UsersService {
    List<AccountDto> getAllUsers();
}

