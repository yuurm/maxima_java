package com.maxima.homework27.dto;

import com.maxima.homework27.models.Actor;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.stream.Collectors;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ActorDto {
    private Long id;
    private String name;

    public static ActorDto from(Actor actor) {
        return ActorDto.builder()
                .id(actor.getId())
                .name(actor.getName())
                .build();
    }

    public static List<ActorDto> from(List<Actor> actors) {
        return actors.stream().map(ActorDto::from).collect(Collectors.toList());
    }
}
