package com.maxima.homework27.dto;

import com.maxima.homework27.models.Actor;
import com.maxima.homework27.models.Film;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.stream.Collectors;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class FilmDto {
    private Long id;
    private String name;

    public static FilmDto from(Film film) {
        return FilmDto.builder()
                .id(film.getId())
                .name(film.getName())
                .build();
    }

    public static List<FilmDto> from(List<Film> films) {
        return films.stream().map(FilmDto::from).collect(Collectors.toList());
    }
}
