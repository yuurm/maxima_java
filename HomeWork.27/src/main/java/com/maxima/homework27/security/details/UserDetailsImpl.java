package com.maxima.homework27.security.details;

import com.maxima.homework27.models.Account;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.Collections;

public class UserDetailsImpl implements UserDetails {

    private final Account account;

    public UserDetailsImpl(Account account) {
        this.account = account;
    }

    // то, что может делать пользователь, можно отождествить с ролью
    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        // берем роль текущего пользователя и кладем в понятный для Spring Security SimpleGrantedAuthority
        SimpleGrantedAuthority authority =  new SimpleGrantedAuthority(account.getRole().name());
        return Collections.singleton(authority);
    }

    @Override
    public String getPassword() {
        return account.getHashPassword();
    }

    @Override
    public String getUsername() {
        return account.getEmail();
    }

    // аккаунт не просрочен
    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    // не заблокирован ли аккаунт
    @Override
    public boolean isAccountNonLocked() {
        return !(account.getState().equals(Account.State.BANNED) || account.getState().equals(Account.State.DELETED));
    }

    // данные для входа не просрочены
    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    // когда аккаунт в принципе доступен
    @Override
    public boolean isEnabled() {
        return account.getState().equals(Account.State.CONFIRMED);
    }
}
