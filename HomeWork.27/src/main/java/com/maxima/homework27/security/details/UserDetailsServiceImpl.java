package com.maxima.homework27.security.details;

import com.maxima.homework27.models.Account;
import com.maxima.homework27.repositories.AccountsRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@RequiredArgsConstructor
@Service("customUserDetailsService")
public class UserDetailsServiceImpl implements UserDetailsService {

    private final AccountsRepository accountsRepository;

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        // достаем пользователя из базы данных
        // если не находим - выбрасываем UsernameNotFound
        Account account = accountsRepository.findByEmail(email).orElseThrow(() ->
                new UsernameNotFoundException("User not found"));
        // оборачиваем пользователя в объект UserDetailsImpl
        return new UserDetailsImpl(account);
    }
}
