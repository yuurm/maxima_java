package com.maxima.homework27.repositories;

import com.maxima.homework27.models.Actor;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ActorsRepository extends JpaRepository<Actor, Long> {
    List<Actor> findAllByFilm(Long id);
    List<Actor> findAllByFilmIsNull();

}
