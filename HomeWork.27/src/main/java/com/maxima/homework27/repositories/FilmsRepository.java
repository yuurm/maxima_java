package com.maxima.homework27.repositories;

import com.maxima.homework27.models.Film;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface FilmsRepository extends JpaRepository<Film, Long> {
    List<Film> findAll();
}
