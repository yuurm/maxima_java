package ru.maxima.reflection.solution.framework;

import ru.maxima.reflection.solution.Certificate;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

/**
 * 02.09.2021
 * 35. Reflection
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class DocumentsFramework {
    public <T extends Document> T generate(Class<T> documentClass, Object ... args) {
        // нам нужно понять, как сопоставить args и поля класса
        List<Class<?>> argsTypes = new ArrayList<>();

        for (Object argument : args) {
            argsTypes.add(argument.getClass());
        }
        // создаем массив для типов аргументов
        Class<?>[] argsTypesAsArray = new Class[argsTypes.size()];
        // перекидываем данные из списка в массив
        argsTypes.toArray(argsTypesAsArray);
        try {
            Constructor<T> documentConstructor = documentClass.getConstructor(argsTypesAsArray);
            // создаем документ с использованием указанного конструктора
            T document = documentConstructor.newInstance(args);
            processDefaultValueAnnotation(document);
            return document;
        } catch (Exception e) {
            throw new IllegalStateException(e);
        }


    }

    private <T extends Document> void processDefaultValueAnnotation(T document) {
        Class<T> documentClass = (Class<T>) document.getClass();
        Field[] fields = documentClass.getDeclaredFields();

        for (Field field : fields) {
            // хотим получить поля, которые помечены аннотацией DefaultValue
            DefaultValue defaultValueAnnotation = field.getAnnotation(DefaultValue.class);
            // если поле field помечено аннотацией DefaultValue
            if (defaultValueAnnotation != null) {
                // забираем значение, которым было помечено это поле
                String value = defaultValueAnnotation.value();
                // указываем возможность задания значения этого поля
                field.setAccessible(true);
                // в конкретный документ в конкретное поле кладем конкретное значение
                try {
                    field.set(document, value);
                } catch (IllegalAccessException e) {
                    throw new IllegalArgumentException(e);
                }
            }
        }
    }

//TODO: В DocumentsFramework реализовать метод
//public void processingCheckRangeAnnotation(Document document)
//Если в объекте document содержится аннотация @CheckRange над полями, при этом у полей
//неверные данные (не соответствуют диапазону) необходимо выбросить исключение.
//Certificate certificate = new Certificate(11);
//documentsFramework.processingCheckRangeAnnotation(certificate);

    public <T extends Document> void processingCheckRangeAnnotation(Certificate document)  {
        Class<T> documentClass = (Class<T>) document.getClass();
        Field[] fields = documentClass.getDeclaredFields();

        for (Field field : fields) {
            // хотим получить поля, которые помечены аннотацией CheckRange
            CheckRange checkRangeAnnotation = field.getAnnotation(CheckRange.class);
            // если поле field помечено аннотацией DefaultValue
            if (checkRangeAnnotation != null) {
                // забираем значение, которым было помечено это поле
                int min = checkRangeAnnotation.min();
                int max = checkRangeAnnotation.max();
                // указываем возможность задания значения этого поля
                field.setAccessible(true);

                //получаем значение поля ActiveYears
                // проверяем его на удовлетворение условию попадания в диапазон

                int tempActiveYears = 0;
                try {
                    tempActiveYears = field.getInt(document);
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }

                if (tempActiveYears <= min || tempActiveYears >= max){
                    throw new IllegalStateException("Годы активности не попадают " +
                            "в заданный диапазон");
                }
            }
        }
    }
}
