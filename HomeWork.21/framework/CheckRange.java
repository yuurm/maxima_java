package ru.maxima.reflection.solution.framework;
//TODO: В проекте 35. Reflection добавить аннотацию @CheckRange

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(value = RetentionPolicy.RUNTIME)
@Target(value = ElementType.FIELD)

public @interface CheckRange {
    int min();
    int max();
}
