package ru.maxima.reflection.solution;

//TODO: Reflection добавить аннотацию @CheckRange
// Пример использования:
// import ru.maxima.reflection.solution.framework.CheckRange;

import ru.maxima.reflection.solution.framework.CheckRange;

public class Certificate {
    @CheckRange(min = 0, max = 10)
    private int activeYears;

    public Certificate(int activeYears) {
        this.activeYears = activeYears;
    }

    public int getActiveYears() {
        return activeYears;
    }

    @Override
    public String toString() {
        return "Certificate{" +
                "activeYears=" + activeYears +
                '}';
    }
}
