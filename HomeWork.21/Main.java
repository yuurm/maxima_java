package ru.maxima.reflection.solution;

import ru.maxima.reflection.solution.framework.DocumentsFramework;

import java.time.LocalDate;
import java.time.LocalDateTime;

public class Main {

    public static void main(String[] args) {
        DocumentsFramework framework = new DocumentsFramework();
        Statement statement = framework.generate(Statement.class, "Сидиков Марсель",
                LocalDate.of(1994, 2, 2));

        Statement statement1 = framework.generate(Statement.class, "Сидиков Марсель");

        Letter letter = framework.generate(Letter.class, "Марсель", "Айрат");

        /*Act act = framework.generate(Act.class,
                LocalDate.of(2021, 9, 7),
                "Акт №1",
                "Акт об акте",
                "ОБЭП");

         */

        //создаем объект сертификата со знчением аргумента - 11
        Certificate certificate = new Certificate(5);
        framework.processingCheckRangeAnnotation(certificate);



        System.out.println(statement);
        System.out.println(statement1);
        System.out.println(letter);
        System.out.println( );
        System.out.println( );
        System.out.println(certificate);
    }
}
