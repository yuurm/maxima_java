package ru.maxima.servlets;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.maxima.config.ApplicationConfig;
import ru.maxima.services.GoodsService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * 20.09.2021
 * 41. Java Servlet Example
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
@WebServlet("/goods")
public class GoodsServlet extends HttpServlet {

    private GoodsService goodsService;

    @Override
    public void init() throws ServletException {
        ApplicationContext context = new AnnotationConfigApplicationContext(ApplicationConfig.class);
        this.goodsService = context.getBean(GoodsService.class);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        PrintWriter writer = response.getWriter();

        writer.println("<h1>List of goods</h1>");
        writer.println("<table>");
        writer.println("    <tr>");
        writer.println("        <th>" + "Name of good" + "</th>");
        writer.println("    </tr>");
        for (String name : goodsService.getAllNames()) {
            writer.println("<tr>");
            writer.println("    <td>" + name + "</td>");
            writer.println("</tr>");
        }
        writer.println("</table>");
    }
}
