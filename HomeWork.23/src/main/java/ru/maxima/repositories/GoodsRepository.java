package ru.maxima.repositories;

import ru.maxima.models.Good;

import java.util.List;

/**
 * 20.08.2021
 * 33. Simple Program with JDBC
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public interface GoodsRepository {
    void save(Good good);
    List<Good> findAll();
}
