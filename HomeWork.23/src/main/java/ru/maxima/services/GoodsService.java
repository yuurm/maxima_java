package ru.maxima.services;

import java.util.List;

/**
 * 20.09.2021
 * 41. Java Servlet Example
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public interface GoodsService {
    void addGood(String name);

    List<String> getAllNames();
}
