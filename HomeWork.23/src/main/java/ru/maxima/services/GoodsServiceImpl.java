package ru.maxima.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.maxima.models.Good;
import ru.maxima.repositories.GoodsRepository;

import java.util.List;
import java.util.stream.Collectors;

/**
 * 06.09.2021
 * 36. Spring Demo
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
@Service
public class GoodsServiceImpl implements GoodsService {

    private final GoodsRepository goodsRepository;

    @Autowired
    public GoodsServiceImpl(GoodsRepository goodsRepository) {
        this.goodsRepository = goodsRepository;
    }


    /*public void addGood(String name) {
        Good good = Good.builder()
                .name(name)
                .build();

        goodsRepository.save(good);
    }**/

    @Override
    public void addGood(String name) {
        Good good = Good.builder()
                .name(name)
                .build();

        goodsRepository.save(good);
    }

    @Override
    public List<String> getAllNames() {
        // a -> a.method() -> A::method
        // a -> System.out.println(a) -> System.out::println
        // a -> x.method(a) -> x::method
        return goodsRepository.findAll().stream().map(Good::getName).collect(Collectors.toList());
    }
}
