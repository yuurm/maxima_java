package ru.maxima.servlets;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * 13.09.2021
 * 41. Java Servlet Example
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class SearchPageServlet extends HttpServlet {
    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        PrintWriter writer = response.getWriter();
        //writer.println("<h1>Hello!</h1>");
        writer.println("<p><i>Введите текст для поиска и получите нужный результат!</i></p>");
        writer.println("<br>");
        writer.println("<form action=http://google.com/search>");
        writer.println("<label for=search><b>Тут надо вводить запрос:</b></label>");
        writer.println("<input id=search type=text name=q placeholder=Введите поисковый запрос>");
        writer.println("<input type=submit name= value=ПОИСК>");
        writer.println("</form>");

    }
}
