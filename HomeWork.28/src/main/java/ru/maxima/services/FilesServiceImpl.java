package ru.maxima.services;

import lombok.RequiredArgsConstructor;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import ru.maxima.models.FileInfo;
import ru.maxima.repositories.FilesInfoRepository;

import javax.servlet.http.HttpServletResponse;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.UUID;

/**
 * 15.11.2021
 * Spring Boot Demo
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
@RequiredArgsConstructor
@Service
public class FilesServiceImpl implements FilesService {

    private final FilesInfoRepository filesInfoRepository;

    @Value("/Volumes/SSD 500GB mac/Users/a1/Desktop/MAXIMA_TEST/")
    private String storageFolder;

    @Override
    public void saveFile(MultipartFile file, String description, Long filmId) {
        String extension = FilenameUtils.getExtension(file.getOriginalFilename());

        FileInfo fileInfo = FileInfo.builder()
                .description(description)
                .mimeType(file.getContentType())
                .originalFileName(file.getOriginalFilename())
                .storageFileName(UUID.randomUUID() + "." + extension)
                .size(file.getSize())
                .build();

        filesInfoRepository.save(fileInfo);

        try {
            Files.copy(file.getInputStream(), Paths.get(storageFolder, fileInfo.getStorageFileName()));
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }


    }

    @Override
    public void addFileToResponse(String fileName, HttpServletResponse response) {
        FileInfo fileInfo = filesInfoRepository.findByStorageFileName(fileName);
        response.setContentType(fileInfo.getMimeType());
        response.setContentLength(fileInfo.getSize().intValue());
        response.setHeader("Content-Disposition", "filename=\"" + fileInfo.getOriginalFileName() + "\"");
        try {
            IOUtils.copy(new FileInputStream(storageFolder + "/" + fileName), response.getOutputStream());
            response.flushBuffer();
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }
}
