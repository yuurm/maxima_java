package ru.maxima.services;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.maxima.dto.ActorDto;
import ru.maxima.dto.FilmDto;
import ru.maxima.exceptions.FilmNotFoundException;
import ru.maxima.models.Actor;
import ru.maxima.models.FileInfo;
import ru.maxima.models.Film;
import ru.maxima.repositories.ActorsRepository;
import ru.maxima.repositories.FilesInfoRepository;
import ru.maxima.repositories.FilmsRepository;

import java.util.List;

@Service
@RequiredArgsConstructor
public class FilmServiceImpl implements FilmService {

    private final ActorsRepository actorsRepository;
    private final FilmsRepository filmsRepository;
    private final FilesInfoRepository filesInfoRepository;

    @Override
    public void addFilm(FilmDto film) {
        Film newFilm = Film.builder()
                .id(film.getId())
                .name(film.getName())
                .year(film.getYear())
                .build();
        filmsRepository.save(newFilm);
    }



    @Override
    public List<FilmDto> getAllFilms() {
        return FilmDto.from(filmsRepository.findAll());
    }

    @Override
    public void addActorToFilm(Long filmId, ActorDto actorForm) {
        Film film = filmsRepository.getById(filmId);
        Actor actor = actorsRepository.getById(actorForm.getId());
        actor.setFilm(film);
        actorsRepository.save(actor);
    }

    @Override
    public List<ActorDto> getActorsByFilm(Long filmId) {
        filmsRepository.findById(filmId).orElseThrow(FilmNotFoundException::new);
        return ActorDto.from(actorsRepository.findAllByFilmId(filmId));
    }

    /*@Override
    public void addPicToFilm(Long filmId, FileInfo fileInfo) {
        Film film = filmsRepository.getById(filmId);
        FileInfo newFile = filesInfoRepository.getById(fileInfo.getId());
        newFile.setFilm(film);
        filesInfoRepository.save(newFile);

    }*/

    @Override
    public List<ActorDto> getActorsWithoutFilm() {
        return ActorDto.from(actorsRepository.findAllByFilmIsNull());
    }
}
