package ru.maxima.services;

import ru.maxima.dto.ActorDto;
import ru.maxima.dto.FilmDto;
import ru.maxima.models.Actor;
import ru.maxima.models.FileInfo;
import ru.maxima.models.Film;

import java.util.List;

public interface FilmService {

    void addFilm(FilmDto film);

    List<FilmDto> getAllFilms();

    void addActorToFilm(Long filmId, ActorDto actor);
    List<ActorDto> getActorsByFilm(Long filmId);

    //void addPicToFilm(Long filmId, FileInfo fileInfo);

    List<ActorDto> getActorsWithoutFilm();

}
