package ru.maxima.services;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.maxima.dto.ActorDto;
import ru.maxima.models.Actor;
import ru.maxima.repositories.ActorsRepository;

import java.util.List;

import static ru.maxima.dto.ActorDto.from;

/**
 * 27.10.2021
 * 46. Spring MVC Extended
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
@Service
@RequiredArgsConstructor
public class ActorServiceImpl implements ActorService {

    private final ActorsRepository actorsRepository;

    @Override
    public void addActor(ActorDto actorDto) {
        Actor newActor = Actor.builder()
                .id(actorDto.getId())
                .firstName(actorDto.getFirstName())
                .lastName(actorDto.getLastName())
                .build();

        actorsRepository.save(newActor);
    }

    @Override
    public List<ActorDto> getAllActors() {
        return from(actorsRepository.findAll());
    }

}
