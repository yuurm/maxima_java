package ru.maxima.services;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.maxima.dto.AccountDto;
import ru.maxima.dto.FilmDto;
import ru.maxima.repositories.AccountsRepository;
import ru.maxima.repositories.FilmsRepository;

import java.util.List;

import static ru.maxima.dto.FilmDto.from;

/**
 * 27.10.2021
 * 46. Spring MVC Extended
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
@Service
@RequiredArgsConstructor
public class SearchServiceImpl implements SearchService {

    private final FilmsRepository filmsRepository;


    @Override
    public List<FilmDto> searchFilmsByName(String name) {
        return from(filmsRepository.findByNameLike("%" + name + "%"));
    }
}
