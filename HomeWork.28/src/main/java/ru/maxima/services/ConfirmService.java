package ru.maxima.services;

/**
 * 18.11.2021
 * Spring Boot Demo
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public interface ConfirmService {
    boolean confirm(String uuid);
}
