package ru.maxima.services;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.maxima.dto.AccountDto;
import ru.maxima.repositories.AccountsRepository;

/**
 * 26.11.2021
 * Spring Boot Demo
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
@RequiredArgsConstructor
@Service
public class ProfileServiceImpl implements ProfileService {

    private final AccountsRepository accountsRepository;

    @Override
    public AccountDto getUser(Long currentUserId) {
        return AccountDto.from(accountsRepository.getById(currentUserId));
    }
}
