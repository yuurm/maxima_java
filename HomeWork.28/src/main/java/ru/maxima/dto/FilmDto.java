package ru.maxima.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.maxima.models.Account;
import ru.maxima.models.Film;

import javax.persistence.Entity;
import java.util.List;
import java.util.stream.Collectors;


@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
public class FilmDto {
    private Long id;
    private String name;
    private Integer year;

    public static FilmDto from(Film film) {
        return FilmDto.builder()
                .id(film.getId())
                .name(film.getName())
                .year(film.getYear())
                .build();
    }

    public static List<FilmDto> from(List<Film> films) {
        return films.stream().map(FilmDto::from).collect(Collectors.toList());
    }
}
