package ru.maxima.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.maxima.dto.ActorDto;
import ru.maxima.dto.FilmDto;
import ru.maxima.services.FilmService;
import ru.maxima.services.FilesService;

/**
 * 01.11.2021
 * Spring Boot Demo
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
@Controller
@RequiredArgsConstructor
@RequestMapping("/films")
public class FilmsController {

    private final FilmService filmService;
    private final FilesService fileService;

    @GetMapping
    public String getFilms(Model model) {
        model.addAttribute("films", filmService.getAllFilms());
        return "films";
    }

    /*
    @PostMapping("/{film-id}/delete")
    public String deleteFilm(@PathVariable("film-id") Long filmId) {
        filmService.deleteFilm(filmId);
        return "redirect:/films";
    }
    */


    @PostMapping
    public String addFilm(FilmDto form) {
        filmService.addFilm(form);
        return "redirect:/films";
    }

    //TODO: fix URL /films

    @GetMapping("/{film-id}/films")
    public String getActorsOfFilmPage(@PathVariable("film-id") Long filmId, Model model) {
        model.addAttribute("actors", filmService.getActorsByFilm(filmId));
        model.addAttribute("actorsWithoutFilm", filmService.getActorsWithoutFilm());
        return "actors_of_films";
    }

    @PostMapping(value = "/{film-id}/actors")
    public String addActor(@PathVariable("film-id") Long filmId, ActorDto actor) {
        filmService.addActorToFilm(filmId, actor);
        return "redirect:/films/" + filmId + "/actors";
    }
}
