package ru.maxima.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 04.12.2021
 * Spring Boot Demo
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
@Controller
@RequestMapping("/error")
public class ErrorsController {
    @GetMapping("/404")
    public String get404Page() {
        return "errors/error404";
    }

    @PostMapping("/500/enfe")
    public String get500ForEntityNotFoundException() {
        return "errors/error500enfe";
    }
}

