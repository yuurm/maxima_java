package ru.maxima.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import ru.maxima.dto.ActorDto;
import ru.maxima.services.ActorService;

import java.util.List;

/**
 * 27.10.2021
 * 46. Spring MVC Extended
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
@Controller
@RequiredArgsConstructor
@RequestMapping("/actors")
public class ActorsController {

    private final ActorService actorService;

    @GetMapping
    public String getActorsPage() {
        return "actors";
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    @ResponseStatus(HttpStatus.CREATED)
    public List<ActorDto> addActor(@RequestBody ActorDto actor) {
        actorService.addActor(actor);
        return actorService.getAllActors();
    }
}
