package ru.maxima.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import ru.maxima.services.FilesService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import java.io.BufferedReader;
import java.io.IOException;
import java.util.Collection;

/**
 * 15.11.2021
 * Spring Boot Demo
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
@RequiredArgsConstructor
@Controller
public class FilesController {

    private final FilesService filesService;

    @GetMapping("films/{film-id}/upload")
    public String getFilesUploadPage() {
        return "file_upload_page";
    }

    @PostMapping("films/{film-id}/upload")
    public String uploadFile(@PathVariable("film-id") Long filmId, @RequestParam("file")  MultipartFile file, @RequestParam("description") String description) {
        filesService.saveFile(file, description, filmId);
        return "file_upload_page";
    }

    @GetMapping("/files/{file-name:.+}")
    public void getFile(@PathVariable("file-name") String fileName, HttpServletResponse response) {
        filesService.addFileToResponse(fileName, response);
    }
}
