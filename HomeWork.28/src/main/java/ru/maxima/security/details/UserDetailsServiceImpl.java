package ru.maxima.security.details;

import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import ru.maxima.models.Account;
import ru.maxima.repositories.AccountsRepository;

import java.util.Optional;
import java.util.function.Supplier;

/**
 * 05.11.2021
 * Spring Boot Demo
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
@RequiredArgsConstructor
@Service("customUserDetailsService")
public class UserDetailsServiceImpl implements UserDetailsService {

    private final AccountsRepository accountsRepository;

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        // достаем пользователя из базы данных
        // если не находим - выбрасываем UsernameNotFound
        Account account = accountsRepository.findByEmail(email).orElseThrow(() ->
                new UsernameNotFoundException("User not found"));
        // оборачиваем пользователя в объект UserDetailsImpl
        return new UserDetailsImpl(account);
    }
}
