package ru.maxima.security.details;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import ru.maxima.models.Account;

import java.util.Collection;
import java.util.Collections;

/**
 * 05.11.2021
 * Spring Boot Demo
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class UserDetailsImpl implements UserDetails {
    private static final long serialVersionUID = -6387859154093794447L;

    //private static final long serialVersionUID = -7654494046714378410L;

    private final Account account;

    public UserDetailsImpl(Account account) {
        this.account = account;
    }

    // то, что может делать пользователь, можно отождествить с ролью
    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        // берем роль текущего пользователя и кладем в понятный для Spring Security SimpleGrantedAuthority
        SimpleGrantedAuthority authority =  new SimpleGrantedAuthority(account.getRole().name());
        return Collections.singleton(authority);
    }

    @Override
    public String getPassword() {
        return account.getHashPassword();
    }

    @Override
    public String getUsername() {
        return account.getEmail();
    }

    // аккаунт не просрочен
    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    // не заблокирован ли аккаунт
    @Override
    public boolean isAccountNonLocked() {
        return !(account.getState().equals(Account.State.BANNED) || account.getState().equals(Account.State.DELETED));
    }

    // данные для входа не просрочены
    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    // когда аккаунт в принципе доступен
    @Override
    public boolean isEnabled() {
        return account.getState().equals(Account.State.CONFIRMED);
    }

    public Account getAccount() {
        return account;
    }

    public Long id() {
        return account.getId();
    }
}
