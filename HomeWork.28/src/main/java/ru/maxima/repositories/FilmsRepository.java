package ru.maxima.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.maxima.models.Account;
import ru.maxima.models.Film;

import java.util.List;
import java.util.Optional;

/**
 * 18.10.2021
 * 42. Java Servlet JSP
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public interface FilmsRepository extends JpaRepository<Film, Long> {
    List<Film> findByNameLike(String email);

    //List<Film> findAll();

}
