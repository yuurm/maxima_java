package ru.maxima.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.maxima.models.Actor;

import java.util.List;

/**
 * 27.10.2021
 * 46. Spring MVC Extended
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public interface ActorsRepository extends JpaRepository<Actor, Long> {
    List<Actor> findAllByFilmId(Long filmId);
    List<Actor> findAllByFilmIsNull();
}
