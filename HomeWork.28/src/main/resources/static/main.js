function addActor(color, model, csrfTokenValue) {
    let body = {
        "first_name": firstname,
        "last_name": lastname
    };

    $.ajax({
        type: "POST",
        url: "/actors",
        headers: {
            'X-CSRF-TOKEN': csrfTokenValue
        },
        data: JSON.stringify(body),
        success: response => renderActorTable(response),
        error: function () {
            alert("Ошибка")
        },
        dataType: "json",
        contentType: "application/json"
    });
}

function renderActorTable(response) {
    let html = '<tr>' +
        '<th>First name</th>' +
        '<th>Last name</th>' +
        '</tr>';

    for (let i = 0; i < response.length; i++) {
        html += '<tr>';
        html += '<td>' + response[i]['first_name'] + '</td>';
        html += '<td>' + response[i]['last_name'] + '</td>';
        html += '</tr>'
    }

    $('#car_table').empty().append(html);
}
