package com.maxima.springbootfilms.services;


import com.maxima.springbootfilms.dto.ActorDto;
import com.maxima.springbootfilms.dto.FilmDto;

import java.util.List;

public interface FilmsService {
    void addFilm(FilmDto film);
    List<FilmDto> getAllFilms();
    void addActorToFilm(Long filmId, ActorDto actor);
    List<ActorDto> getActorsByFilm(Long filmId);
    List<ActorDto> getActorsWithoutFilm();

}
