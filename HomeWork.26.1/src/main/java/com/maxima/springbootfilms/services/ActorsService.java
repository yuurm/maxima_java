package com.maxima.springbootfilms.services;



import com.maxima.springbootfilms.dto.ActorDto;

import java.util.List;

public interface ActorsService {
    void addActor(ActorDto actor);
    List<ActorDto> getAllActors();

}
