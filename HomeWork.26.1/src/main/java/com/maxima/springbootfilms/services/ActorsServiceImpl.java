package com.maxima.springbootfilms.services;

import com.maxima.springbootfilms.dto.ActorDto;
import com.maxima.springbootfilms.models.Actor;
import com.maxima.springbootfilms.repositories.ActorsRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;


import java.util.List;

import static com.maxima.springbootfilms.dto.ActorDto.from;



@Service
@RequiredArgsConstructor
public class ActorsServiceImpl implements ActorsService {

    private final ActorsRepository actorsRepository;


    @Override
    public void addActor(ActorDto actor) {
        Actor newActor = Actor.builder()
                .id(actor.getId())
                .name(actor.getName())
                .build();
        actorsRepository.save(newActor);


    }

    @Override
    public List<ActorDto> getAllActors() {
        return from(actorsRepository.findAll());
    }


}
