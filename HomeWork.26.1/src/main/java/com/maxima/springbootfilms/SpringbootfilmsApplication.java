package com.maxima.springbootfilms;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootfilmsApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringbootfilmsApplication.class, args);
    }

}
