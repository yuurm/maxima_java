package com.maxima.springbootfilms.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import ru.maxima.dto.ActorDto;
import ru.maxima.dto.FilmDto;
import ru.maxima.services.FilmsService;

@Controller
public class FilmsController {
    @Autowired
    private FilmsService filmsService;

    @RequestMapping("/films")
    public String getFilms(Model model) {
        model.addAttribute("films", filmsService);
        return "films";
    }

    @RequestMapping("/films/{film-id}/actors")
    public String getActorsOfMoviePage(@PathVariable("film-id") Long filmId, Model model) {
        model.addAttribute("actors", filmsService.getActorsByFilm(filmId));
        model.addAttribute("actors_free", filmsService.getActorsWithoutFilm());
        return "actors_in_film";
    }

    @RequestMapping(value = "/films/{film-id}/actors", method = RequestMethod.POST)
    public String addActorToMovie(@PathVariable("film-id") Long filmId, ActorDto actor) {
        filmsService.addActorToFilm(filmId, actor);
        return "redirect:/films/" + filmId + "/actors";
    }

    @RequestMapping(value = "/films", method = RequestMethod.POST)
    public String addMovie(FilmDto form) {
        filmsService.addFilm(form);
        return "redirect:/films";
    }

}
