package com.maxima.springbootfilms.models;

/*
Реализовать страницу, которая выводит список фильмов - /films

На данной странице можно JSON-запросом добавить фильм - POST /films

Пока каждому фильму можно посмотреть список актеров фильма - /films/{film-id}/actors

При этом, можно добавить актера с помощью HTTP-формы

Аналогично нашему заданию, но код писать самостоятельно! Можно открыть мой код и перепечатывать.
Код не копируем!

 */

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
@Entity

public class Film {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private String name;

    @OneToMany(mappedBy = "film")
    private List<Actor> actors;



}
