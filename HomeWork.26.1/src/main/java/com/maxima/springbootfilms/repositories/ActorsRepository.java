package com.maxima.springbootfilms.repositories;

import com.maxima.springbootfilms.models.Actor;
import org.springframework.data.jpa.repository.JpaRepository;


import java.util.List;

public interface ActorsRepository extends JpaRepository<Actor, Long> {
    List<Actor> findAllByFilm(Long filmId);
    List<Actor> findAllByFilmIsNull();
}
