package com.maxima.springbootfilms.repositories;

import com.maxima.springbootfilms.models.Film;
import org.springframework.data.jpa.repository.JpaRepository;


import java.util.List;

public interface FilmsRepository extends JpaRepository<Film, Long> {
   List<Film> findAll();
}
