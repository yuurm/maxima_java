package com.maxima.springbootfilms.dto;

import com.maxima.springbootfilms.models.Actor;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


import java.util.List;
import java.util.stream.Collectors;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
public class ActorDto {
    private long id;
    private String name;

    public static ActorDto from(Actor actor) {
        return  ActorDto.builder()
                .id(actor.getId())
                .name(actor.getName())
                .build();
    }

    public static List<ActorDto> from(List<Actor> actors) {
        return actors.stream().map(ActorDto::from).collect(Collectors.toList());
    }

}
