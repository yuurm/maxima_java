package ru.maxima.dto;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.maxima.models.Actor;
import ru.maxima.models.Film;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class FilmDto {
    private Long id;
    private String name;

    public static FilmDto from(Film film){
        return FilmDto.builder()
                .id(film.getId())
                .name(film.getName())
                .build();
    }

    public static List<FilmDto> from(List<Film> films){
        return films.stream().map(FilmDto::from).collect(Collectors.toList());
    }



}
