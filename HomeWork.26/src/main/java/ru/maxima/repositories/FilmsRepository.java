package ru.maxima.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.maxima.models.Film;

import java.util.List;

public interface FilmsRepository extends JpaRepository<Film, Long> {
   List<Film> findAll();
}
