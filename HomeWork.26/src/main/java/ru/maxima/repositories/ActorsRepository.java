package ru.maxima.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.maxima.models.Actor;

import java.util.List;
import java.util.Optional;

public interface ActorsRepository extends JpaRepository<Actor, Long> {
    List<Actor> findAllByFilm(Long filmId);
    List<Actor> findAllByFilmIsNull();
}
