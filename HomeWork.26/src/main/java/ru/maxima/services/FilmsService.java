package ru.maxima.services;

import ru.maxima.dto.ActorDto;
import ru.maxima.dto.FilmDto;

import java.util.List;

public interface FilmsService {
    void addFilm(FilmDto film);
    List<FilmDto> getAllFilms();
    void addActorToFilm(Long filmId, ActorDto actor);
    List<ActorDto> getActorsByFilm(Long filmId);
    List<ActorDto> getActorsWithoutFilm();

}
