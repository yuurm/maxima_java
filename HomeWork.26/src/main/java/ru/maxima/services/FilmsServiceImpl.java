package ru.maxima.services;


import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.maxima.dto.ActorDto;
import ru.maxima.dto.FilmDto;
import ru.maxima.models.Actor;
import ru.maxima.models.Film;
import ru.maxima.repositories.ActorsRepository;
import ru.maxima.repositories.FilmsRepository;

import java.util.List;

import static ru.maxima.dto.ActorDto.from;
import static ru.maxima.dto.FilmDto.from;

@Service
@RequiredArgsConstructor
public class FilmsServiceImpl implements FilmsService {

    private final ActorsRepository actorsRepository;
    private final FilmsRepository filmsRepository;

    @Override
    public void addFilm(FilmDto film) {
        Film newFilm = Film.builder()
                .id(film.getId())
                .name(film.getName())
                .build();
        filmsRepository.save(newFilm);
    }

    @Override
    public List<FilmDto> getAllFilms() {
        return from(filmsRepository.findAll());
    }

    @Override
    public void addActorToFilm(Long filmId, ActorDto actorForm) {
        Film film = filmsRepository.getById(filmId);
        Actor actor = actorsRepository.getById(actorForm.getId());
        actor.setFilm(film);
        actorsRepository.save(actor);
    }

    @Override
    public List<ActorDto> getActorsByFilm(Long filmId) {
        return from(actorsRepository.findAllByFilm(filmId));
    }

    @Override
    public List<ActorDto> getActorsWithoutFilm() {
        return from(actorsRepository.findAllByFilmIsNull());
    }
}
