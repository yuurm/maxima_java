package ru.maxima.services;

import ru.maxima.dto.ActorDto;
import ru.maxima.dto.FilmDto;
import ru.maxima.models.Actor;

import java.util.List;

public interface ActorsService {
    void addActor(ActorDto actor);
    List<ActorDto> getAllActors();

}
