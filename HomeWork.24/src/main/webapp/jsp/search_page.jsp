<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<script>
    function searchGoods(name) {
        let request = new XMLHttpRequest();

        request.open('GET', '/searchByName?name=' + name, false);

        request.send();

        if (request.status !== 200) {
            alert("Ошибка!")
        } else {
            let html = '<tr>' +
                '<th>Id</th>' +
                '<th>Name</th>' +
                '</tr>';


            let response = JSON.parse(request.response);

            for (let i = 0; i < response['goods'].length; i++) {
                html += '<tr>';
                html += '<td>' + response['goods'][i]['id'] + '</td>';
                html += '<td>' + response['goods'][i]['name'] + '</td>';
                html += '</tr>'
            }

            document.getElementById('good_table').innerHTML = html;
        }
    }
</script>
<body>
<label for="name">Enter name for search</label>
<input id="name" name="name" placeholder="Name" onkeyup="searchGoods(document.getElementById('name').value)">
<br>
<table id="good_table">

</table>
</body>
</html>
