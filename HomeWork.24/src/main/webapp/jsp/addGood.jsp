<%--
  Created by IntelliJ IDEA.
  User: Yuri
  Date: 30.09.2021
  Time: 08:39
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<script>
    function addGood(name) {
        let body = {
            "name": name,
            //"model": model
        };

        let request = new XMLHttpRequest();

        request.open('POST', '/addGood', false);
        request.setRequestHeader("Content-Type", "application/json");
        request.send(JSON.stringify(body));

        if (request.status !== 201) {
            alert("Ошибка!")
        }

    }
</script>
<body>
        <label for="name">Name of good:</label>
        <input id="name" name="name" placeholder="Example: milk"/>
        <button onclick="addGood(
        document.getElementById('name').value)">Add
</button>


</body>
</html>
