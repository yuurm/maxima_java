package ru.maxima.servlets;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.context.ApplicationContext;
import ru.maxima.dto.GoodDto;
import ru.maxima.services.GoodsService;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * 20.09.2021
 * 41. Java Servlet Example
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
@WebServlet("/goods")
public class GoodsServlet extends HttpServlet {
    private GoodsService goodsService;
    private ObjectMapper objectMapper;

    @Override
    public void init(ServletConfig config) throws ServletException {
        ServletContext servletContext = config.getServletContext();
        ApplicationContext applicationContext = (ApplicationContext) servletContext.getAttribute("springContext");
        this.objectMapper = applicationContext.getBean(ObjectMapper.class);
        this.goodsService = applicationContext.getBean(GoodsService.class);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        List<GoodDto> goods = goodsService.getAllGoods();
        request.setAttribute("goods", goods);
        request.getRequestDispatcher("/jsp/goods.jsp").forward(request, response);
//        PrintWriter writer = response.getWriter();
//
//        writer.println("<h1>Email</h1>");
//        writer.println("<table>");
//        writer.println("    <tr>");
//        writer.println("        <th>" + "Email of user" + "</th>");
//        writer.println("    </tr>");
//        for (String email : usersService.getAllEmails()) {
//            writer.println("<tr>");
//            writer.println("    <td>" + email + "</td>");
//            writer.println("</tr>");
//        }
//        writer.println("</table>");
    }
}
