package ru.maxima.servlets;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.context.ApplicationContext;
import ru.maxima.dto.GoodDto;
import ru.maxima.services.GoodsService;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 20.09.2021
 * 41. Java Servlet Example
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
@WebServlet("/addGood")
public class AddGoodServlet extends HttpServlet {
    private GoodsService goodsService;
    private ObjectMapper objectMapper;

    @Override
    public void init(ServletConfig config) throws ServletException {
        ServletContext servletContext = config.getServletContext();
        ApplicationContext applicationContext = (ApplicationContext) servletContext.getAttribute("springContext");
        this.goodsService = applicationContext.getBean(GoodsService.class);
        this.objectMapper = applicationContext.getBean(ObjectMapper.class);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//        response.getWriter().println("<h1>Sign Up</h1>\n" +
//                "<p>Please enter your data</p>\n" +
//                "<br>\n" +
//                "<form method=\"post\">\n" +
//                "\t<label for=\"email\">Enter email here:</label>\n" +
//                "\t<input id=\"email\" type=\"text\" name=\"email\" placeholder=\"Your email\">\n" +
//                "\t<label for=\"password\">Enter password here:</label>\n" +
//                "\t<input id=\"password\" type=\"text\" name=\"password\" placeholder=\"Your password\">\n" +
//                "\t<input type=\"submit\" name=\"\" value=\"Sign Up\">\n" +
//                "</form>");
        request.getRequestDispatcher("/jsp/addGood.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String goodAsJson = request.getReader().readLine();
        GoodDto good = objectMapper.readValue(goodAsJson, GoodDto.class);
        System.out.println(good);
        response.setStatus(201);

    }
}
