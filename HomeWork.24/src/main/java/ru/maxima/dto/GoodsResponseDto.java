package ru.maxima.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

/**
 * 23.09.2021
 * 42. Java Servlet JSP
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
@Data
@AllArgsConstructor
public class GoodsResponseDto {
    private List<GoodDto> goods;
}
