package ru.maxima.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.maxima.dto.GoodDto;
import ru.maxima.models.Good;
import ru.maxima.repositories.GoodsRepository;

import java.util.List;
import java.util.stream.Collectors;

import static ru.maxima.dto.GoodDto.from;

/**
 * 06.09.2021
 * 36. Spring Demo
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
@Service
public class GoodsServiceImpl implements GoodsService {

    private final GoodsRepository goodsRepository;

    @Autowired
    public GoodsServiceImpl(GoodsRepository goodsRepository) {
        this.goodsRepository = goodsRepository;
    }


    @Override
    public void addGood(GoodDto goodDTO) {
        Good good = Good.builder()
                //.id(goodDTO.getId())
                .name(goodDTO.getName())
                //.password(password)
                .build();

        goodsRepository.save(good);
    }

    @Override
    public List<GoodDto> getAllGoods() {
        // a -> a.method() -> A::method
        // a -> System.out.println(a) -> System.out::println
        // a -> x.method(a) -> x::method
        //return goodsRepository.findAll().stream().map(Good::getEmail).collect(Collectors.toList());
        return from(goodsRepository.findAll());
    }



    @Override
    public List<GoodDto> searchGoodByName(String name) {
        return from(goodsRepository.searchByGoodsName(name));
    }
}
