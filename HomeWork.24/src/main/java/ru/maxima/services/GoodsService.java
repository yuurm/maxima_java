package ru.maxima.services;

import ru.maxima.dto.GoodDto;

import java.util.List;

/**
 * 20.09.2021
 * 41. Java Servlet Example
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public interface GoodsService {
    void addGood(GoodDto goodDTO);

    //List<String> getAllEmails();
    List<GoodDto> getAllGoods();

    List<GoodDto> searchGoodByName(String name);
}
