package ru.maxima.repositories;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import ru.maxima.models.Good;

import javax.sql.DataSource;
import java.util.Collections;
import java.util.List;

/**
 * 20.08.2021
 * 33. Simple Program with JDBC
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
@Repository
public class GoodsRepositoryNamedParameterJdbcTemplateImpl implements GoodsRepository {

    //language=SQL
    private static final String SQL_INSERT =
            "insert into good(name) values (:name) RETURNING id";

    //language=SQL
    private static final String SQL_SELECT_ALL =
            "select * from good order by id";

    //language=SQL
    private static final String SQL_SELECT_ALL_BY_NAME_LIKE =
            "select * from good where name ilike :name";

    private final NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    private static final RowMapper<Good> accountRowMapper = (row, rowNumber) -> Good.builder()
            .id(row.getLong("id"))
            .name(row.getString("name"))
            .build();

    @Autowired
    public GoodsRepositoryNamedParameterJdbcTemplateImpl(DataSource dataSource) {
        this.namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
    }

    @Override
    public void save(Good good) {
        // данный объект запоминает сгенерированные базой данных ключи
        KeyHolder keyHolder = new GeneratedKeyHolder();
        namedParameterJdbcTemplate.update(SQL_INSERT, (new MapSqlParameterSource()
                //.addValue("name", good.getName())
                .addValue("name", good.getName())), keyHolder, new String[]{"id"});
        good.setId(keyHolder.getKey().longValue());
    }

    @Override
    public List<Good> findAll() {
        return namedParameterJdbcTemplate.query(SQL_SELECT_ALL, accountRowMapper);
    }

    @Override
    public List<Good> searchByGoodsName(String name) {
        return namedParameterJdbcTemplate.query(SQL_SELECT_ALL_BY_NAME_LIKE,
                Collections.singletonMap("name", "%" + name + "%"),
                accountRowMapper);
    }


}
