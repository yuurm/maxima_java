package ru.maxima.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.maxima.models.Good;

import java.util.List;
import java.util.Optional;

/**
 * 18.10.2021
 * 42. Java Servlet JSP
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public interface GoodsRepository extends JpaRepository<Good, Long> {
    //Optional<Good> findByEmail(String email);

    //List<Good> findByNameLike(String name);
    List<Good> findAll();
}
