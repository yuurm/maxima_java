package ru.maxima.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.maxima.services.GoodsService;

/**
 * 21.10.2021
 * 46. Spring MVC Extended
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
@Controller
public class GoodsController {

    @Autowired
    private GoodsService goodsService;

//    @RequestMapping("/users")
//    public ModelAndView getUsers() {
//        ModelAndView modelAndView = new ModelAndView("users_page");
//        modelAndView.addObject("accounts", usersService.getAllUsers());
//        return modelAndView;
//    }

    @RequestMapping("/goods")
    public String getGoods(Model model) {
       model.addAttribute("goods", goodsService.getAllGoods());
       return "goods_page";
    }
}
