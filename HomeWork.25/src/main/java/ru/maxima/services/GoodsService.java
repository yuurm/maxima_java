package ru.maxima.services;

import ru.maxima.dto.GoodDto;

import java.util.List;

/**
 * 20.09.2021
 * 41. Java Servlet Example
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public interface GoodsService {
    List<GoodDto> getAllGoods();
}
