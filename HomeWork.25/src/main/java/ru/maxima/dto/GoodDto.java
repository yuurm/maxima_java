package ru.maxima.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.maxima.models.Good;

import java.util.List;
import java.util.stream.Collectors;

/**
 * 23.09.2021
 * 42. Java Servlet JSP
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
public class GoodDto {
    private Long id;
    private String name;

    public static GoodDto from(Good good) {
        return GoodDto.builder()
                .id(good.getId())
                .name(good.getName())
                .build();
    }

    public static List<GoodDto> from(List<Good> goods) {
        return goods.stream().map(GoodDto::from).collect(Collectors.toList());
    }
}
